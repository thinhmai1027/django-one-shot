from django.shortcuts import render

from todos.models import TodoList
from django.views.generic.list import ListView


# Create your views here.
class TodoListView(ListView):
    model = TodoList
    template_name = "todos/list.html"
    paginate_by = 2
